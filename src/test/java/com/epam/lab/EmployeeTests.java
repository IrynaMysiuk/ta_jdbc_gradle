package com.epam.lab;

import com.epam.lab.daoimpl.EmployeeDaolmpl;
import com.epam.lab.entities.Employee;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.List;


public class EmployeeTests {
    private static Logger logger = LogManager.getLogger(App.class);
    private int expectedId = 967;
    private int initialEmployee;
    private EmployeeDaolmpl sdi;

    @BeforeClass
    public void addTestData() {
        sdi = new EmployeeDaolmpl();
        sdi.createEmployeeTable();
        initialEmployee = (int) Math.random();
        Employee employee = new Employee()
                .setId(initialEmployee)
                .setLastName("Googlay").setFirstName("Bohdan")
                .setMiddleName("Severynovych").setPoint("m")
                .setBirthdayDay("1935-12-01").setParentsName("Googlay Severyn Valentinovych")
                .setPhone("11122233").setPassport("CC0909CC")
                .setRecordBookNumber("3114029c").setArrivalDate("2011-09-01");
        sdi.insert(employee);
    }

    @Test
    public void checkInsertEmployee() {
        EmployeeDaolmpl sdi = new EmployeeDaolmpl();
        logger.info("Create table");
        sdi.createEmployeeTable();
        logger.info("Insert a new record");
        Employee employee = new Employee()
                .setId(expectedId)
                .setLastName("Lopatka").setFirstName("Ostap")
                .setMiddleName("Severynovych").setPoint("m")
                .setBirthdayDay("1995-12-01").setParentsName("Lopanka Severyn Valentinovych")
                .setPhone("11122233").setPassport("CC0909CC")
                .setRecordBookNumber("3114049c").setArrivalDate("2016-09-01");
        sdi.insert(employee);
        logger.info("Select by id");
        Employee employeeSelect = sdi.selectById(expectedId);
        logger.info(employee.getId() + ", " + employee.getFirstName() + ", " + employee.getLastName());
        Assert.assertEquals(expectedId, employeeSelect.getId(), "Employee was not added");
    }

    @Test
    public void checkUpdateEmployee() {
        String expectedLastName = "Grabla";
        EmployeeDaolmpl sdi = new EmployeeDaolmpl();
        logger.info("Select by id");
        Employee employeeSelect = sdi.selectById(expectedId);
        Employee personUpdate = employeeSelect
                .setLastName(expectedLastName);
        sdi.update(personUpdate, employeeSelect.getId());
        Assert.assertEquals(personUpdate.getLastName(), expectedLastName, "Employee was not update");
    }

    @Test
    public void checkResults() {
        EmployeeDaolmpl sdi = new EmployeeDaolmpl();
        logger.info("Select all persons");
        List<Employee> employees = sdi.selectAll();
        for (Employee p : employees) {
            logger.info(p.getId() + ", " + p.getFirstName() + ", " + p.getLastName());
        }
        Assert.assertTrue(employees.size() > 0, "Select all employees");
    }

    @AfterClass
    public void deleteData() {
        EmployeeDaolmpl sdi = new EmployeeDaolmpl();
        sdi.delete(expectedId);
        sdi.delete(initialEmployee);
    }
}
