package com.epam.lab.util;

import java.sql.Connection;
import java.sql.DriverManager;

public class ConnectionConfiguration {

    public static Connection getConnection() {
        DataProp props = new DataProp();
        Connection connection = null;
        try {
            Class.forName(props.getDBDriver());
            connection = DriverManager.getConnection(props.getDBUrl(), props.getDBUserName(), props.getDBPassword());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return connection;
    }
}