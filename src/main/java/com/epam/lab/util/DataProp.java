package com.epam.lab.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class DataProp {
    public static final String DB_URL="db_url";
    public static final String DB_USERNAME="db_username";
    public static final String DB_PASSWORD="db_password";
    public static final String DB_DRIVER="db_driver";
    private Properties props = new Properties();

    public DataProp() {
        FileInputStream fis;

        try {
            fis = new FileInputStream("src/main/resources/db.properties");
            props.load(fis);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getDBUrl() {
        return props.getProperty(DB_URL);
    }
    public String getDBUserName() {
        return props.getProperty(DB_USERNAME);
    }
    public String getDBPassword() {
        return props.getProperty(DB_PASSWORD);
    }
    public String getDBDriver() {
        return props.getProperty(DB_DRIVER);
    }
}
